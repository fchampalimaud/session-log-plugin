# !/usr/bin/python3
# -*- coding: utf-8 -*-

""" session_window.py

"""

import logging

from pysettings import conf

if conf.PYFORMS_USE_QT5:
	from PyQt5.QtWidgets import QMessageBox
	from PyQt5.QtCore import QTimer, QEventLoop
else:
	from PyQt4.QtGui import QMessageBox
	from PyQt4.QtCore import QTimer, QEventLoop

from pysettings import conf

from pyforms import BaseWidget
from pyforms.Controls import ControlProgress
from pyforms.Controls import ControlTextArea

from pyforms_generic_editor.com.messaging.board_message import BoardMessage

logger = logging.getLogger(__name__)


class SessionLogWindow(BaseWidget):
	""" Plugin main window """

	def __init__(self, session):
		BaseWidget.__init__(self, session.name)
		self.layout().setContentsMargins(5, 5, 5, 5)

		self.session = session

		self._log = ControlTextArea()
		self._progress = ControlProgress('Loading', 0, 1, 100)

		self._formset = [
			'_log',
			'_progress'
		]

		self._history_index = 0
		self._log.readOnly = True
		self._progress.hide()

		self._timer = QTimer()
		self._timer.timeout.connect(self.read_message_queue)

	def show(self):
		# Prevent the call to be recursive because of the mdi_area
		if hasattr(self, '_show_called'):
			BaseWidget.show(self)
			return
		self._show_called = True
		self.mainwindow.mdi_area += self
		del self._show_called

		self.read_message_queue(True)
		self._timer.start(conf.SESSIONLOG_PLUGIN_REFRESH_RATE)

	def hide(self):
		self._timer.stop()

	def beforeClose(self):
		self._timer.stop()
		return False

	def read_message_queue(self, update_gui=False):
		""" Update board queue and retrieve most recent messages """
		messages_history = self.session.messages_history
		recent_history = messages_history[self._history_index:]

		if update_gui:
			self._progress.show()
			self._progress.value = 0
		try:
			for message in recent_history:

				# if issubclass(type(message), BoardMessage):

				message = "{idx} | {type} | {message}".format(idx=self._history_index,
				                                              type=message.MESSAGE_TYPE_ALIAS,
				                                              message=message.format_string)

				self._log += message
				QEventLoop()

				if update_gui:
					self._progress += 1
					if self._progress.value >= 99: self._progress.value = 0

				self._history_index += 1
		except Exception as err:
			if hasattr(self, '_timer'):
				self._timer.stop()
			logger.error(str(err), exc_info=True)
			QMessageBox.critical(self, "Error",
			                     "Unexpected error while loading session history. Pleas see log for more details.")

		if update_gui:
			self._progress.hide()

	@property
	def mainwindow(self):
		return self.session.mainwindow

	@property
	def title(self):
		return BaseWidget.title.fget(self)

	@title.setter
	def title(self, value):
		BaseWidget.title.fset(self, 'Log: {0}'.format(value))
